#include "ppp-notify.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <sysexits.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <syslog.h>

#define NOTIFY_SOCKET	"/run/ppp-eventd"

static bool endswith (char const *str, char const *suffix)
{
	size_t		l_str = strlen(str);
	size_t		l_sfx = strlen(suffix);

	if (l_str < l_sfx)
		return false;

	return memcmp(str + l_str - l_sfx, suffix, l_sfx) == 0;
}

static void unit_test(void)
{
	assert(endswith("foo.bar", "bar"));
	assert(endswith("foo.bar", "foo.bar"));
	assert(!endswith("bar", "foo.bar"));
	assert(!endswith("foo.bar", "Xbar"));
}

static void do_log_cmdline(int argc, char *argv[])
{
	size_t	len = 1;
	char	*buf;
	char	*p;

	for (int i = 1; i < argc; ++i)
		len += sizeof " ''" + strlen(argv[i]) - 1;

	buf = malloc(len);
	if (!buf)
		abort();

	p = buf;
	for (int i = 1; i < argc; ++i) {
		*p++ = ' ';
		*p++ = '"';
		p    = stpcpy(p, argv[i]);
		*p++ = '"';
	}

	*p = '\0';

	openlog("ppp-helper", 0, LOG_UUCP);
	syslog(LOG_INFO, "%s:%s", argv[0], buf);

	openlog("ppp-helper", 0, LOG_UUCP | LOG_PERROR);
	
	free(buf);
}

static int fill_common(struct ppp_notify_msg *msg,
		       char const *device, char const *ifname,
		       char const *speed)
{
	char		*err;
	char const	*tmp;

	if (strlen(device) >= sizeof msg->device) {
		syslog(LOG_ERR, "invalid device '%s'", device);
		return EX_USAGE;
	}
	strcpy(msg->device, device);

	if (strlen(ifname) >= sizeof msg->ifname) {
		syslog(LOG_ERR, "invalid ifname '%s'", ifname);
		return EX_USAGE;
	}
	strcpy(msg->ifname, ifname);

	if (!speed) {
		msg->speed = 0;
	} else {
		unsigned long	num;

		num = strtoul(speed, &err, 10);
		if (*err || (uint32_t)num != num) {
			syslog(LOG_ERR, "bad speed '%s'", speed);
			return EX_USAGE;
		}
		msg->speed = htobe32(num);
	}

	tmp = getenv("BYTES_SENT");
	if (!tmp) {
		msg->bytes_sent = 0;
	} else {
		uint64_t	num;

		num = strtoull(tmp, &err, 10);
		if (*err || (uint64_t)num != num) {
			syslog(LOG_ERR, "bad $BYTES_SEND '%s'", tmp);
			return EX_USAGE;
		}
		msg->bytes_sent = num;
	}

	tmp = getenv("BYTES_RCVD");
	if (!tmp) {
		msg->bytes_rcvd = 0;
	} else {
		uint64_t	num;

		num = strtoull(tmp, &err, 10);
		if (*err || (uint64_t)num != num) {
			syslog(LOG_ERR, "bad $BYTES_RCVD '%s'", tmp);
			return EX_USAGE;
		}
		msg->bytes_rcvd = num;
	}

	return EX_OK;
}

static int fill_msg_ip4(struct ppp_notify_msg *msg, char *argv[], size_t argc)
{
	int		rc;

	/* 'ipparam' is optional */
	if (argc != 6 && argc != 5) {
		syslog(LOG_ERR, "IP4: bad parameters");
		return EX_USAGE;
	}

	rc = fill_common(msg, argv[0], argv[1], argv[2]);
	if (rc != EX_OK)
		return rc;

	rc = inet_pton(AF_INET, argv[3], &msg->ip4.local);
	if (rc < 0) {
		syslog(LOG_ERR, "IP4: failed to convert local ip '%s': %s",
		       argv[3], strerror(errno));
		return EX_USAGE;
	}

	rc = inet_pton(AF_INET, argv[4], &msg->ip4.remote);
	if (rc < 0) {
		syslog(LOG_ERR, "IP4: failed to convert remote ip '%s': %s",
		       argv[4], strerror(errno));
		return EX_USAGE;
	}

	/* TODO: ipparm is ignored */

	return EX_OK;
}

static int fill_msg_ip6(struct ppp_notify_msg *msg, char *argv[], size_t argc)
{
	int		rc;

	/* 'ipparam' is optional */
	if (argc != 6 && argc != 5) {
		syslog(LOG_ERR, "IP6: bad parameters");
		return EX_USAGE;
	}

	rc = fill_common(msg, argv[0], argv[1], argv[2]);
	if (rc != EX_OK)
		return rc;

	rc = inet_pton(AF_INET6, argv[3], &msg->ip6.local);
	if (rc < 0) {
		syslog(LOG_ERR, "IP6: failed to convert local ip '%s': %s",
		       argv[3], strerror(errno));
		return EX_USAGE;
	}

	rc = inet_pton(AF_INET6, argv[4], &msg->ip6.remote);
	if (rc < 0) {
		syslog(LOG_ERR, "IP6: failed to convert remote ip '%s': %s",
		       argv[4], strerror(errno));
		return EX_USAGE;
	}

	/* TODO: ipparm is ignored */

	return EX_OK;
}

int main(int argc, char *argv[])
{
	struct ppp_notify_msg		msg = {};
	struct ppp_notify_resp		resp;
	int				rc;
	enum ppp_notify_event		event;
	int				fd;
	struct sockaddr_un		addr = {
		.sun_family	= AF_UNIX,
		.sun_path	= NOTIFY_SOCKET
	};

	unit_test();

	do_log_cmdline(argc, argv);

	if (endswith(argv[0], "ip-up"))
		event = PPP_EV_IP_UP;
	else if (endswith(argv[0], "ip-pre-up"))
		event = PPP_EV_IP_PRE_UP;
	else if (endswith(argv[0], "ip-down"))
		event = PPP_EV_IP_DOWN;
	else if (endswith(argv[0], "auth-up"))
		event = PPP_EV_AUTH_UP;
	else if (endswith(argv[0], "auth-down"))
		event = PPP_EV_AUTH_DOWN;
	else if (endswith(argv[0], "ip6-up"))
		event = PPP_EV_IP6_UP;
	else if (endswith(argv[0], "ip6-down"))
		event = PPP_EV_IP6_DOWN;
	else if (endswith(argv[0], "ipx-up"))
		event = PPP_EV_IP6_UP;
	else if (endswith(argv[0], "ipx-down"))
		event = PPP_EV_IP6_DOWN;
	else if (endswith(argv[0], "init"))
		event = PPP_EV_INIT;
	else {
		syslog(LOG_ERR, "Unsupported called name '%s'", argv[0]);
		return EX_USAGE;
	}

	switch (event) {
	case PPP_EV_IP_UP:
	case PPP_EV_IP_DOWN:
	case PPP_EV_IP_PRE_UP:
		rc = fill_msg_ip4(&msg, argv + 1, argc - 1);
		break;
	case PPP_EV_IP6_UP:
	case PPP_EV_IP6_DOWN:
		rc = fill_msg_ip6(&msg, argv + 1, argc - 1);
		break;
	case PPP_EV_AUTH_UP:
	case PPP_EV_AUTH_DOWN:
		syslog(LOG_CRIT, "IMPLEMENT ME: auth not supported");
		rc = EX_SOFTWARE;
		break;
	case PPP_EV_IPX_UP:
	case PPP_EV_IPX_DOWN:
		syslog(LOG_CRIT, "IMPLEMENT ME: ipx not supported");
		rc = EX_SOFTWARE;
		break;
	case PPP_EV_INIT:
		rc = EX_OK;
		break;
	}

	if (rc != EX_OK)
		return rc;

	msg.event = event;

	fd = socket(AF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0);
	if (fd < 0) {
		syslog(LOG_ERR, "socket(<notify>): %s", strerror(errno));
		return EX_OSERR;
	}

	rc = connect(fd, (void *)&addr, sizeof addr);
	if (rc < 0) {
		syslog(LOG_ERR, "connect(<notify>): %s", strerror(errno));
		rc = EX_OSERR;
		goto out;
	}

	/* TODO: handle short writes */
	rc = send(fd, &msg, sizeof msg, MSG_NOSIGNAL);
	if (rc < 0) {
		syslog(LOG_ERR, "send(<notify>): %s", strerror(errno));
		rc = EX_OSERR;
		goto out;
	}
	if ((size_t)rc != sizeof msg) {
		syslog(LOG_ERR, "short write");
		rc = EX_OSERR;
		goto out;
	}

	rc = recv(fd, &resp, sizeof resp, MSG_WAITALL);
	if (rc < 0) {
		syslog(LOG_ERR, "recv(<notify>): %s", strerror(errno));
		rc = EX_OSERR;
		goto out;
	}

	if (resp.code != 0) {
		syslog(LOG_ERR, "remote site failed with %d", resp.code);
		rc = EX_DATAERR;
		goto out;
	}

	rc = EX_OK;

out:
	close(fd);
	return rc;
}
