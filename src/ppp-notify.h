/*	--*- c -*--
 * Copyright (C) 2018 Enrico Scholz <enrico.scholz@ensc.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef H_ENSC_PPP_NOTIFY_H
#define H_ENSC_PPP_NOTIFY_H

#include <stdint.h>

enum ppp_notify_event {
	PPP_EV_AUTH_UP,
	PPP_EV_AUTH_DOWN,
	PPP_EV_IP_PRE_UP,
	PPP_EV_IP_UP,
	PPP_EV_IP_DOWN,
	PPP_EV_IP6_UP,
	PPP_EV_IP6_DOWN,
	PPP_EV_IPX_UP,
	PPP_EV_IPX_DOWN,
	PPP_EV_INIT,
};

typedef uint32_t	be32_t;
typedef uint64_t	be64_t;

#pragma pack(push,1)
struct ppp_notify_msg {
	uint8_t		event;
	uint8_t		_rsrv[7];
	char		device[32];
	char		ifname[32];
	be32_t		speed;
	be64_t		bytes_sent;
	be64_t		bytes_rcvd;

	union {
		struct {
			be32_t	local;
			be32_t	remote;
		}		ip4;

		struct {
			uint8_t	local[16];
			uint8_t	remote[16];
		}		ip6;

		struct {
			char	peer[32];
			char	user[32];
		}		auth;

		/* TODO: IPX? */
	};
};
#pragma pack(pop)

#pragma pack(push,1)
struct ppp_notify_resp {
	uint8_t		code;
};
#pragma pack(pop)

#endif	/* H_ENSC_PPP_NOTIFY_H */
