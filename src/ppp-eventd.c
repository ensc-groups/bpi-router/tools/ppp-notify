/*	--*- c -*--
 * Copyright (C) 2018 Enrico Scholz <enrico.scholz@ensc.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <sysexits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/wait.h>

#include <arpa/inet.h>

#include <ensc-lib/sd-listen.h>
#include <ensc-lib/sd-notify.h>
#include <ensc-lib/compiler.h>

#include "ppp-notify.h"

#define  SCRIPT_DIR	"/etc/ppp-event.d"

struct ip_handler_info {
	char const	*iptables;
};

static struct ppp_notify_msg const	msg_template;

struct msg_context {
	char const	*id;

	char		device[sizeof msg_template.device + 1];
	char		ifname[sizeof msg_template.ifname + 1];

	char		local[INET6_ADDRSTRLEN];
	char		remote[INET6_ADDRSTRLEN];
	char		actual[INET6_ADDRSTRLEN];

	char		counter[sizeof(uint64_t) * 3 + 2];
};

static struct in_addr	g_actual_ip4;
static bool		g_have_ip4;
static struct in6_addr	g_actual_ip6;
static bool		g_have_ip6;

static uint64_t		g_counters[PPP_EV_INIT + 1];

static void fill_msg_context(struct msg_context *ctx,
			     struct ppp_notify_msg const *msg)
{
	static char const * const	IDS[] = {
		[PPP_EV_AUTH_DOWN]	= "auth-down",
		[PPP_EV_AUTH_UP]	= "auth-up",
		[PPP_EV_IP_PRE_UP]	= "ip-pre-up",
		[PPP_EV_IP_UP]		= "ip-up",
		[PPP_EV_IP_DOWN]	= "ip-down",
		[PPP_EV_IP6_UP]		= "ip6-up",
		[PPP_EV_IP6_DOWN]	= "ip6-down",
		[PPP_EV_INIT]		= "init",
	};
	void const	*local = NULL;
	void const	*remote = NULL;
	void const	*actual = NULL;
	int		af;

	memcpy(ctx->device, msg->device, sizeof msg->device);
	ctx->device[sizeof ctx->device - 1] = '\0';

	memcpy(ctx->ifname, msg->ifname, sizeof msg->ifname);
	ctx->ifname[sizeof ctx->ifname - 1] = '\0';

	if (ctx->device[0] == '.' || strchr(ctx->device, '/') != NULL) {
		fprintf(stderr, "invalid device name '%s'\n", ctx->device);
		ctx->device[0] = '\0';
	}

	if (ctx->ifname[0] == '.' || strchr(ctx->ifname, '/') != NULL) {
		fprintf(stderr, "invalid ifname '%s'\n", ctx->ifname);
		ctx->ifname[0] = '\0';
	}

	if (msg->event < ARRAY_SIZE(IDS))
		ctx->id = IDS[msg->event];

	switch (msg->event) {
	case PPP_EV_IP_PRE_UP:
	case PPP_EV_IP_UP:
	case PPP_EV_IP_DOWN:
		af = AF_INET;
		local = &msg->ip4.local;
		remote = &msg->ip4.remote;

		if (g_have_ip4)
			actual = &g_actual_ip4;
		break;

	case PPP_EV_IP6_UP:
	case PPP_EV_IP6_DOWN:
		af = AF_INET6;
		local = &msg->ip6.local;
		remote = &msg->ip6.remote;

		if (g_have_ip6)
			actual = &g_actual_ip6;
		break;

	default:
		break;
	}

	if (msg->event >= ARRAY_SIZE(g_counters)) {
		ctx->counter[0] = '\0';
	} else {
		snprintf(ctx->counter, sizeof ctx->counter, "%" PRIu64,
			 g_counters[msg->event]);

		g_counters[msg->event] += 1;
	}

	if (local)
		inet_ntop(af, local, ctx->local, sizeof ctx->local);
	else
		ctx->local[0] = '\0';

	if (remote)
		inet_ntop(af, remote, ctx->remote, sizeof ctx->remote);
	else
		ctx->remote[0] = '\0';

	if (actual)
		inet_ntop(af, actual, ctx->actual, sizeof ctx->actual);
	else
		ctx->actual[0] = '\0';
}

static int run_script(struct msg_context *ctx, char const * const args[])
{
	char			*program = NULL;
	int			rc;
	int			fd[2] = { -1, -1 };
	pid_t			pid;
	int			status;
	ssize_t			l;

	rc = asprintf(&program, "%s/%s", SCRIPT_DIR, ctx->id);
	if (rc < 0) {
		perror("asprintf()");
		program = NULL;
		goto err;
	}

	if (access(program, X_OK) < 0 && errno == ENOENT) {
		/* ignore missing scripts */
		free(program);
		return 0;
	}

	rc = pipe2(fd, O_CLOEXEC);
	if (rc < 0) {
		perror("pipe2()");
		goto err;
	}

	pid = fork();
	if (pid < 0) {
		perror("fork()");
		goto err;

	} else if (pid == 0) {
		int	ret;

		/* child */
		close(fd[0]);		/* read */
		execvp(program, (char * const *)args);
		ret = errno;
		write(fd[1], &ret, sizeof ret);
		close(fd[1]);
		_exit(EXIT_FAILURE);
	}

	free(program);
	close(fd[1]);

	rc = waitpid(pid, &status, 0);
	if (rc < 0) {
		perror("waitpid()");
		close(fd[0]);
		return -1;
	}

	l = read(fd[0], &status, sizeof status);
	close(fd[0]);
	if (l > 0) {
		fprintf(stderr, "failed to execute script: %s\n",
			strerror(status));
		return -1;
	}

	if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
		fprintf(stderr, "script failed with %04x\n", status);
		return -1;
	}

	return 0;

err:
	if (fd[0] >= 0)
		close(fd[0]);

	if (fd[1] >= 0)
		close(fd[1]);

	free(program);

	return -1;
}

static int run_generic_script(struct msg_context *ctx)
{
	char const * const	args[] = {
		ctx->id,
		ctx->device,
		ctx->local, ctx->remote, ctx->actual,
		ctx->counter,
		NULL,
	};

	return run_script(ctx, args);
}

static int run_ip_pre_up(struct ppp_notify_msg const *msg,
		     struct msg_context	*ctx)
{
	if (run_generic_script(ctx) < 0)
		return -1;

	return 0;
}

static int run_ip_up(struct ppp_notify_msg const *msg,
		     struct msg_context	*ctx)
{
	if (run_generic_script(ctx) < 0)
		return -1;

	g_have_ip4 = true;
	g_actual_ip4.s_addr = msg->ip4.local;

	return 0;
}

static int run_ip_down(struct ppp_notify_msg const *msg,
		       struct msg_context *ctx)
{
	if (run_generic_script(ctx) < 0)
		return -1;

	g_have_ip4 = false;

	return 0;
}

static int run_ip6_up(struct ppp_notify_msg const *msg,
		      struct msg_context *ctx)
{
	if (run_generic_script(ctx) < 0)
		return -1;

	_Static_assert(sizeof g_actual_ip6.s6_addr ==
		       sizeof msg->ip6.local, "bad ipv6 layout");

	g_have_ip6 = true;
	memcpy(g_actual_ip6.s6_addr, msg->ip6.local, sizeof msg->ip6.local);

	return 0;
}

static int run_ip6_down(struct ppp_notify_msg const *msg,
			struct msg_context *ctx)
{
	if (run_generic_script(ctx) < 0)
		return -1;

	g_have_ip6 = false;

	return 0;
}

static int run_init(struct ppp_notify_msg const *msg,
		    struct msg_context *ctx)
{
	char		ip4[INET_ADDRSTRLEN];
	char		ip6[INET6_ADDRSTRLEN];

	char const * const	args[] = {
		ctx->id, ip4, ip6, ctx->counter, NULL
	};

	if (g_have_ip4)
		inet_ntop(AF_INET,  &g_actual_ip4, ip4, sizeof ip4);
	else
		ip4[0] = '\0';

	if (g_have_ip6)
		inet_ntop(AF_INET6, &g_actual_ip6, ip6, sizeof ip6);
	else
		ip6[0] = '\0';

	if (run_script(ctx, args) < 0)
		return -1;

	return 0;
}

static int handle_msg(struct ppp_notify_msg const *msg)
{
	struct msg_context	ctx;
	int			rc;

	fill_msg_context(&ctx, msg);

	switch (msg->event) {
	case PPP_EV_IP_PRE_UP:
		rc = run_ip_pre_up(msg, &ctx);
		break;

	case PPP_EV_IP_UP:
		rc = run_ip_up(msg, &ctx);
		break;

	case PPP_EV_IP_DOWN:
		rc = run_ip_down(msg, &ctx);
		break;

	case PPP_EV_IP6_UP:
		rc = run_ip6_up(msg, &ctx);
		break;

	case PPP_EV_IP6_DOWN:
		rc = run_ip6_down(msg, &ctx);
		break;

	case PPP_EV_INIT:
		rc = run_init(msg, &ctx);
		break;

	default:
		fprintf(stderr, "event %d not supported", msg->event);
		rc = 1;
		break;
	}

	return rc;
}

int main(int argc, char *argv[])
{
	int	num_fds;
	int	listen_fd;

	num_fds = sd_listen_fds(1);
	if (num_fds != 1) {
		fprintf(stderr, "bad listen fd setup %d\n", num_fds);
		return EX_USAGE;
	}

	listen_fd = SD_LISTEN_FDS_START + 0;

	sd_notify(0, "READY=1");

	for (;;) {
		int			fd;
		struct ppp_notify_msg	msg;
		struct ppp_notify_resp	resp = {};
		ssize_t			l;

		fd = accept4(listen_fd, NULL, NULL, SOCK_CLOEXEC);
		if (fd < 0) {
			fprintf(stderr, "accept(): %s\n", strerror(errno));
			break;
		}

		l = recv(fd, &msg, sizeof msg, MSG_WAITALL);
		if (l < 0) {
			fprintf(stderr, "recv():  %s\n", strerror(errno));
			goto next;
		}
		if ((size_t)l != sizeof msg) {
			fprintf(stderr, "bad recv; %zu vs. %zu\n", l, sizeof resp);
			goto next;
		}

		resp.code = handle_msg(&msg);

		l = send(fd, &resp, sizeof resp, MSG_NOSIGNAL);
		if (l < 0) {
			fprintf(stderr, "send():  %s\n", strerror(errno));
			goto next;
		}
		if ((size_t)l != sizeof resp) {
			fprintf(stderr, "bad send; %zu vs. %zu\n", l, sizeof resp);
			goto next;
		}

	next:
		close(fd);
	}

	return EX_OSERR;
}
