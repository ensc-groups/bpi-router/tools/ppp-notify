srcdir ?= $(dir $(firstword ${MAKEFILE_LIST}))
VPATH = ${srcdir}

INSTALL = install
INSTALL_BIN = ${INSTALL} -p -m 0755
INSTALL_DATA = ${INSTALL} -p -m 0644
LN_S = ln -s
MKDIR_P = ${INSTALL} -d -m 0755

AM_CPPFLAGS = -I${srcdir}
AM_CFLAGS = -std=gnu11 -Wall -W -Wno-unused-parameter

compile_link = ${CC} -o $@ \
	${AM_CPPFLAGS} ${CPPFLAGS} \
	${AM_CFLAGS} ${CFLAGS} \
	${AM_LDFLAGS} ${LDFLAGS} \
	$1 \
	${LIBS}

prefix ?=			/usr/local
libexecdir ?=			${prefix}/libexec
ppplibexecdir ?=		${libexecdir}/ppp
sbindir ?=			${prefix}/sbin
systemd_system_unitdir ?=	${prefix}/lib/systemd/system

ppp-notify_SOURCES = \
	src/ppp-notify.c \
	src/ppp-notify.h

ppp-eventd_SOURCES = \
	src/ppp-notify.h \
	src/ppp-eventd.c \
	ensc-lib/sd-listen.c \
	ensc-lib/sd-listen.h \
	ensc-lib/sd-notify.c \
	ensc-lib/sd-notify.h \

all:	ppp-notify ppp-eventd

ppp-notify:	${ppp-notify_SOURCES}
	$(call compile_link,$(filter %.c,$^))

ppp-eventd:	${ppp-eventd_SOURCES}
	$(call compile_link,$(filter %.c,$^))

install:	.install-libexec-ppp .install-sbin .install-systemd

.install-libexec-ppp:	ppp-notify
	${INSTALL_BIN} -D $< ${DESTDIR}${ppplibexecdir}/ppp-notify
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ip-up
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ip-down
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ip-pre-up
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ip6-up
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ip6-down
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ipx-up
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/ipx-down
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/auth-up
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/auth-down
	${LN_S} ppp-notify ${DESTDIR}${ppplibexecdir}/init

.install-sbin:		ppp-eventd
	${INSTALL_BIN} -D $< ${DESTDIR}${sbindir}/ppp-eventd

.install-systemd:	contrib/ppp-eventd.service contrib/ppp-eventd.socket
	${MKDIR_P} ${DESTDIR}${systemd_system_unitdir}
	${INSTALL_DATA} $^ ${DESTDIR}${systemd_system_unitdir}/

clean:
	rm -f ppp-notify ppp-eventd
